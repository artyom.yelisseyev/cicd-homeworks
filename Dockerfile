FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /simple-app/src/main.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
